<?php
/******************************************************************************
* Grade Report Webservice - External Library
*
* The webservices functions for the Grade Report Webservice plugin.
*
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/externallib.php");

class local_gradeservices_external extends external_api {
     /**
     * Returns description of QUIZ_IDS_PASSED parameters.
     * @return external_function_parameters
     */
	 public static function quiz_ids_passed_parameters() {
    	$config = get_config('local/gradeservices');
        return new external_function_parameters(array(
            	'transid'	=> new external_value(PARAM_INT, 'transaction id', VALUE_REQUIRED),
            	'courseid' 	=> new external_value(PARAM_INT, 'id of course',VALUE_REQUIRED),
                'studentid'	=> new external_value(PARAM_ALPHANUM, $config->studentidname.' for student',VALUE_REQUIRED)
        ));
    }

	/**
     * Passed quizids web service function.
     * @param int $transid		= Transaction id
     * @param int $courseid 	= Moodle course id
     * @param int $studentid	= Student id
     * @return array $quizids 	= Quizids of passed quizzes for given student in course
     */
    public static function quiz_ids_passed($transid,$courseid,$studentid) {
        global $CFG, $DB;

		require_once($CFG->dirroot."/local/gradeservices/locallib.php");
        $params = self::validate_parameters(self::quiz_results_parameters(),
        			array('transid'=>$transid,'courseid'=>$courseid,'studentid'=>$studentid));
       	$context = context_course::instance($params['courseid']);
    	self::validate_context($context);

        // If course exists then check capability - context is invalid otherwise
    	require_capability('mod/quiz:viewreports', $context);

        $user = validate_user($context, $params['studentid']);

        /* Return results of quizzes */
        $results['transid'] = $params['transid'];
        $results['quizids'] = mod_ids_passed('quiz',$courseid,$user->id);

        return $results;
    }

    /**
     * Returns description of method return values
     * @return
     */
	public static function quiz_ids_passed_returns() {
		$config = get_config('local/gradeservices');
		return new external_single_structure(
			array(
				'transid' 	=> new external_value(PARAM_INT, 'transaction id'),
                'quizids' 	=> new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_TEXT, 'idnumber for passed quiz'),
                            'date' => new external_value(PARAM_TEXT, 'timestamp of grade modified')
                        )
                    )
                )
            )
        );
    }

    /**
     * Returns description of DELETE_QUIZ parameters.
     * @return external_function_parameters
     */
    public static function delete_quiz_attempt_parameters(){
        $config = get_config('local/gradeservices');
        return new external_function_parameters(array(
            'transid'	    => new external_value(PARAM_INT, 'transaction id', VALUE_REQUIRED),
            'courseid' 	    => new external_value(PARAM_INT, 'id of course',VALUE_REQUIRED),
            'studentid'	    => new external_value(PARAM_ALPHANUM, $config->studentidname.' for student',VALUE_REQUIRED),
            'quizids'       => new external_multiple_structure(
                                    new external_value(PARAM_INT, 'id number of quiz', VALUE_REQUIRED)
                                )
        ));
    }

    /**
     * @param int $transid     = transation id
     * @param int $courseid    = Moodle course id
     * @param int $studentid   = student to delete
     * @param array $quizids   = quiz ids to delete
     * @return external_function_parameters
     */
    public static function delete_quiz_attempt($transid, $courseid, $studentid, $quizids){
        global $CFG, $DB;

        require_once($CFG->dirroot."/local/gradeservices/locallib.php");

        $params = self::validate_parameters(self::delete_quiz_attempt_parameters(),
            array('transid'=>$transid,'courseid'=>$courseid,'studentid'=>$studentid, 'quizids'=>$quizids));

        $context = context_course::instance($params['courseid']);
        self::validate_context($context);

        require_capability('mod/quiz:deleteattempts', $context);

        $user = validate_user($context, $params['studentid']);
        $results['attempts_deleted'] = delete_user_quiz_attempts($user, $courseid, $quizids);

        $results['transid'] = $params['transid'];

        return $results;
    }

    public static function delete_quiz_attempt_returns(){
        $config = get_config('local/gradeservices');
        return new external_single_structure(
            array(
                'transid'   => new external_value(PARAM_INT, 'transation id'),
                'attempts_deleted'  => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'quizid' => new external_value(PARAM_TEXT, 'id number of quiz'),
                            'attempts' => new external_value(PARAM_INT, 'number attempts deleted')
                        )
                    )
                )
            )
        );
    }

    /**
     * Returns description of QUIZ_RESULTS parameters.
     * @return external_function_parameters
     */
    public static function quiz_results_parameters() {
    	$config = get_config('local/gradeservices');
        return new external_function_parameters(array(
            	'transid'	=> new external_value(PARAM_INT, 'transaction id', VALUE_REQUIRED),
            	'courseid' 	=> new external_value(PARAM_INT, 'id of course',VALUE_REQUIRED),
                'studentid'	=> new external_value(PARAM_ALPHANUM, $config->studentidname.' for student',VALUE_REQUIRED)
        ));
    }

	/**
     * Quiz results reporting web service function.
     * @param int $transid		= Transaction id
     * @param int $courseid 	= Moodle course id
     * @param int $studentid	= Student id
     * @return array $results 	= Quiz results for given student in course
     */
    public static function quiz_results($transid,$courseid,$studentid) {
        global $CFG, $DB;

		require_once($CFG->dirroot."/local/gradeservices/locallib.php");

        $params = self::validate_parameters(self::quiz_results_parameters(),
        			array('transid'=>$transid,'courseid'=>$courseid,'studentid'=>$studentid));

       	$courseid = $params['courseid'];
       	$context = get_context_instance(CONTEXT_COURSE, $courseid);
        try{
        	self::validate_context($context);
        }catch(Exception $e){
        	throw new webservice_parameter_exception('invalidextparam',"No course with given id ($courseid).");
        }
        // If course exists then check capability - context is invalid otherwise
    	require_capability('mod/quiz:viewreports', $context);

        $user = validate_user($context, $params['studentid']);

        /* Return results of quizzes */
        $results['transid'] = $params['transid'];
        $results['quizzes'] = mod_grade_results('quiz',$courseid,$user->id);

        return $results;
    }

    /**
     * Returns description of method return values
     * @return
     */
	public static function quiz_results_returns() {
		return new external_single_structure(
			array(
				'transid' 	=> new external_value(PARAM_INT, 'transaction id'),
                'quizzes' 	=> new external_multiple_structure(
								new external_single_structure(array(
				                	'quizid' => new external_value(PARAM_TEXT, 'idnumber for quiz'),
									'quizname' => new external_value(PARAM_TEXT, 'name of quiz'),
				                	'result' => new external_value(PARAM_TEXT, 'result of quiz')
				                ))
			    )
            )
        );
    }
}

?>
