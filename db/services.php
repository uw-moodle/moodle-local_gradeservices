<?php
/******************************************************************************
* Grade Report Webservice - Services Definitions
*
* The webservices definitions for the Grade Report Webservice plugin.
* 
* Author: Nick Koeppen
******************************************************************************/

$functions = array(
    'mod_quiz_results' => array(
        'classname'   		=> 'local_gradeservices_external',
        'methodname'  		=> 'quiz_results',
        'classpath'   		=> 'local/gradeservices/externallib.php',
        'description' 		=> 'Report results of all quizzes for a given course and student.',
        'type'        		=> 'read',
		'capabilities'		=> 'mod/quiz:viewreports',
    ),
    
    'mod_quiz_ids_passed' => array(
        'classname'   		=> 'local_gradeservices_external',
        'methodname'  		=> 'quiz_ids_passed',
        'classpath'   		=> 'local/gradeservices/externallib.php',
        'description' 		=> 'Returns quizids for only passed quizzes for a given course and student.',
        'type'        		=> 'read',
		'capabilities'		=> 'mod/quiz:viewreports',
    ),

    'mod_quiz_delete_attempt' => array(
        'classname'         => 'local_gradeservices_external',
        'methodname'        => 'delete_quiz_attempt',
        'classpath'   		=> 'local/gradeservices/externallib.php',
        'description' 		=> 'Delete quiz attempt for a student and course',
        'type'        		=> 'write',
        'capabilities'		=> 'mod/quiz:deleteattempts',
    )
);
?>