<?php
/******************************************************************************
* Grade Report Webservice - Global Settings
*
* Global (site-wide) configuration settings for grade report webservice.
* 
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die;

$modname = 'local_gradeservices';
$plugin = 'local/gradeservices';

if ($hassiteconfig) {
    $settings = new admin_settingpage($modname, 'Grade Report Services');
    $ADMIN->add('localplugins', $settings);
    
	$config = get_config($plugin);
	/* Add email addresses domain */
	if(isset($config->studentidfield)){
		if($domain = strstr($USER->{$config->studentidfield}, "@")){
			set_config('emaildomain',$domain,$plugin);
		}else{
			unset_config('emaildomain',$plugin);
		}
	}

	$configs = array();
	
	$configs[] = new admin_setting_configcheckbox("allowmodid",
				get_string('allowmodid', $modname), 
				get_string('allowmodiddesc', $modname),0);
	//$USER fields (that contain strings) for student mapping
	$options = array();
	foreach((array)$USER as $field => $value){
		if(gettype($value) == "string"){
			$options[$field] = $field.(($value)?' (e.g.,"'.$value.'")':'');
		}
	}
	$configs[] = new admin_setting_configselect("studentidfield", 
					get_string('studentidfield', $modname), 
					get_string('studentidfielddesc', $modname), 'idnumber', $options);
	$configs[] = new admin_setting_configtext("studentidname",
	        		get_string('studentidname', $modname), 
	        		get_string('studentidnamedesc', $modname), 'PVI', PARAM_TEXT);
	        			
	foreach ($configs as $config) {
	   $config->plugin = $plugin;
	   $settings->add($config);
	}
}

?>
