<?php
/******************************************************************************
* Grade Report Webservice - Local Library
*
* The backwork functions for the Grade Report Webservice plugin.
*
* Author: Nick Koeppen
******************************************************************************/

defined('MOODLE_INTERNAL') || die;
require_once($CFG->dirroot . '/mod/quiz/locallib.php');

function mod_ids_passed($mod,$courseid,$userid){
	$grades = retreive_grades($mod,$courseid,$userid);
	$config = get_config('local/gradeservices');

	$ids = array();

	foreach ($grades as $grade){
		if ($grade->result >= $grade->gradepass){
			if($grade->idnumber){
				$ids[] = array('id'=>$grade->idnumber, 'date'=>$grade->date);
			}elseif($config->allowmodid){
				$ids[] = "No idnumber ($mod id=$grade->modid)";
			}
	    }
	}

	return $ids;
}

function mod_grade_results($mod,$courseid,$userid){
	$grades = retreive_grades($mod,$courseid,$userid);
	$config = get_config('local/gradeservices');

	foreach ($grades as $name => $grade){
		if($grade->idnumber){
			$results[] = array($mod.'id'=>$grade->idnumber,$mod.'name'=>$name,'result'=>$grade->letter);
		}elseif($config->allowmodid){
			$results[] = array($mod.'id'=>"No idnumber ($mod id=$grade->modid)",$mod.'name'=>$name,'result'=>$grade->letter);
		}
	}

	return $results;
}

function validate_user($context, $studentid){
	global $DB;

	// Retrieve database configuration
    $config = get_config('local/gradeservices');
	$field = $config->studentidfield;
	$value = $studentid;
	if(!empty($config->emaildomain)){
		$value .= $config->emaildomain;
	}

	// Check that user exists and is enrolled in the class
	if(!($user = $DB->get_record('user', array($field=>$value)))) {
        throw new webservice_parameter_exception('invalidextparam',"No user with given $field ($studentid).");
	}
	if(!is_enrolled($context,$user)){
        throw new webservice_parameter_exception('invalidextparam',"Student ($field = $studentid) not enrolled in course.");
	}

	return $user;
}

function retreive_grades($mod,$courseid,$userid){
	global $CFG, $DB;
	require_once("$CFG->libdir/gradelib.php");

    /* Retrieve grade items for modules in course */
	$params = array('courseid'=>$courseid,'itemtype'=>'mod','itemmodule'=>$mod,'outcomeid'=>null);
    if(!($grade_items = grade_item::fetch_all($params))){
    	$course = $DB->get_record('course',array('id'=>$courseid));
        throw new webservice_parameter_exception('invalidextparam',"No $mod modules for $course->fullname (id=$courseid).");
    }

   	/* Retrieve grades */
    $grades = array();
    foreach ($grade_items as $grade_item){
    	$grade = new stdClass();
    	$grade->modid = $grade_item->iteminstance;
    	$grade->idnumber = $grade_item->idnumber;
	    $grade->gradepass = $grade_item->gradepass;

    	$grade_info = grade_grade::fetch_users_grades($grade_item, $userid,false);
    	if (!isset($grade_info[$userid])) {
    	    continue;
    	}
    	$grade->result = $grade_info[$userid]->finalgrade;
    	$grade->letter = grade_format_gradevalue_letter($grade->result,$grade_item);
        $grade->date = $grade_info[$userid]->timemodified;
    	$grades[$grade_item->itemname] = $grade;
    }
    return $grades;
}

function delete_user_quiz_attempts($user, $courseid, $quizids){
	global $DB;

	$quizes = $DB->get_records('quiz', array('course'=>$courseid));

	if (!$quizes || count($quizes) == 0){
		throw new moodle_exception('No quizes for this course');
	}

	foreach($quizes as $quiz){
		$cm = get_coursemodule_from_instance('quiz', $quiz->id);
		if (in_array($cm->idnumber, $quizids)){

			$attempts = quiz_get_user_attempts($quiz->id, $user->id, 'all');
			$results[] = array('quizid' => $cm->idnumber, 'attempts'=>count($attempts));

			foreach($attempts as $attempt){
				quiz_delete_attempt($attempt, $quiz);
			}
		}
	}

	return $results;

}
?>
