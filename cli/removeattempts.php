<?php
/**
 * Created by IntelliJ IDEA.
 * User: dsgraham
 * Date: 8/10/16
 * Time: 3:45 PM
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot . '/mod/quiz/locallib.php');


list($options, $unrecognized) = cli_get_params(array('help'=> false, 'quizid'=>false, 'courseid'=>false, 'daysold'=>false),
                                               array('h'=>'help','q'=>'quizid', 'c'=>'courseid', 'd'=>'daysold'));

if ($options['help'] || !($options['courseid'] && $options['quizid'])) {
    $help = <<<EOL
Delete all attempts for a quiz before a certain date.

Options:
-c --courseid=INTEGER          Course ID for quiz.
-d --daysold=INTEGER           Delete all of the attempts that are older that this many days
                               100 days old is default
-q --quizid=INTEGER            Quiz Id to delete attempts for, or 'all' for every quiz in course.
-h, --help                     Print out this help.

Example:
\$sudo -u www-data /usr/bin/php local/gradeservices/cli/removeattempts.php --courseid=2 --quizid=2 --daysold=100\n
EOL;
    echo $help;
    die;
}


$quizid = $options['quizid'];
$courseid = $options['courseid'];
$daysold = $options['daysold'];
if (!$daysold){
    $daysold = 100;
    mtrace('No date specified, defaulting to 100 days old');
}

$date = time() - ($daysold * 24 * 60 * 60);

$age = (time() - $date) / (60 * 60 * 24); // 60 Sec * 60 Min * 24 Hrs
mtrace('Deleting quiz attempts that are over '. sprintf('%0.1f', $age) . ' days old');


if ($quizid === 'all') {
    $quizzes = $DB->get_records('quiz', array('course'=>$courseid));
} else {
    $quizzes = $DB->get_record('quiz', array('id'=>$quizid, 'course'=>$courseid), '*', MUST_EXIST);
    $quizzes = array($quizzes);
}
if (!$quizzes){
    mtrace("No quiz found with id = $quizid.");
    die;
}

foreach ($quizzes as $quiz) {
    $params = array();
    $params['quizid'] = $quiz->id;
    $params['date'] = $date;
    $attempts = $DB->get_records_select('quiz_attempts',
                                        'quiz = :quizid AND timestart < :date',$params, 'attempt ASC');

    mtrace('Found '.count($attempts)." records to delete in quiz {$quiz->id}: \"{$quiz->name}\"");

    foreach($attempts as $attempt){
        quiz_delete_attempt($attempt, $quiz);
    }
}
