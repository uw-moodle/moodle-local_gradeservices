<?php
$string['pluginname']           = 'Grade Services';

$string['studentidfield'] 		= 'Student ID Field';
$string['studentidfielddesc'] 	= 'The USER id field where the desired student identification information is stored.  If the field is an email address, only search by username as domains will be automatically added. Example: netID@wisc.edu only requires netID to be input.';
$string['studentidname'] 		= 'Student ID Name';
$string['studentidnamedesc'] 	= 'The display name of the information (PVI,Campus ID,netID) stored in the given user field.';
$string['allowmodid'] 			= 'Return Module IDs';
$string['allowmodiddesc'] 		= 'Allow the database id values to be returned if idnumber fields are not specified.  This is meant to aid in debugging by identifying modules with no idnumber.';
?>